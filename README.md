# learn, a documentation generator

## Facts

- POSIX sh

- Works on non-GNU systems and BSD systems

- Less than 100 SLOC

## How to use

Put your markdown files in `docs/`, you can, if you
want to be organized, create sub-directories 
(but docs/foo/bar/foobar.md wont work for now)!

Generate the documentation:

	$ ./learn.sh

For more information:

	$ ./learn.sh --help


